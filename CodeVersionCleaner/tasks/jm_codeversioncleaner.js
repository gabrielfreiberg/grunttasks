'use strict';

module.exports = function(grunt) {
    grunt.registerTask('codeversioncleaner', 'Deletes Old Code Version Folders', function() {
        var options = grunt.config('codeversioncleaner.options');

        //NOTE: async() is a Grunt function that causes the main control thread to look for 
        //  the corresponding execution of the done() function to be called before
        //  exiting the current function - allows for this code to wait for an asynchronous 
        //  command to complete - otherwise this code would complete too soon and no work would
        //  be done.  If done() is not eventually invoked then the code will 'hang'.
        
        var done = this.async();  //Grunt synchronization

        console.log('WebDav Server: ' + options.webdav_server);
        
        if (options.max_code_versions) {
            console.log('Number of code versions to keep: ' + options.max_code_versions);    
        }        
        
        //This code retrieves the current active code version from an instance then
        //  inside a 'callback' function it gets the list of 'extra' code version folders that should 
        //  be deleted from the instance and deletes these code version folders from the instance
        //  one at a time.
        //
        //  NOTE: Node is asynchronous, so to delete each code version folder, requires a way to synchronize
        //  the multiple HTTP DELETE commands so that that the main control logic will wait until all the
        //  HTTP DELETE commands complete - that is what the 'complete' array is for - once the 
        //  'complete' array has a length matching the number of code version folders to delete
        //  then the 'done()' method is called so that the main control logic will then stop waiting.
        
        getActiveCodeVersion(options, processCodeVersions);

        function processCodeVersions (activeCodeVersion) {            
            getCodeVersionFolders(options, activeCodeVersion, function(codeVersions) {
                var complete = [];
                if (codeVersions.length) {
                    codeVersions.forEach(function(codeVersion) {
                        deleteCodeVersionFolder(options, codeVersion, function() {
                            if (complete.length === codeVersions.length - 1) {
                                done();
                            } else {
                                complete.push(true);
                            }                                        
                        });                    
                    });
                } else {
                    console.log('No code version folders found to delete.');
                    done();
                }        
            });
        }
    }); 
}

/**
Return the active code version from the '.version' file - this file contains the current
active code version folder name. The active code version name will be the first code version 
listed in this file (once you get past the header).  

An example of the contents of the .version file is shown below where 'version1' is the 
active code version.

###########################################
# Generated file, do not edit.
# Copyright (c) 2014 by Demandware, Inc.
###########################################
version1/1417621045221
New_Version_12032014-5/1417620956025
version1/1416863958556
555/1416863682344       

@param {Object} options - The options defined in the Gruntfile
@param {Function} callback - this is called to return the active code version name
*/
function getActiveCodeVersion(options, callback) {
    var https = require('https');
    var acv_re = /(.*)\/(.*)/gm;
    var matches = [];
    var match;

    var req = https.get({
        rejectUnauthorized: false,
        host: options.webdav_server,
        path: '/on/demandware.servlet/webdav/Sites/Cartridges/.version',
        auth: options.webdav_username + ":" + options.webdav_password
    }, function(resp) {
        var str = '';
        resp.on('data', function(chunk) {
            str += chunk;
        });
        resp.on('end', function() {
            do {
                match = acv_re.exec(str);                     
                match && matches.push(match);
            } while (match);
            
            if (matches.length) {
                for (var i = 0; i < matches.length; i++) {
                    //Active code version will be the first match found 
                    var activeCodeVersion = matches[i][1];
                    console.log('Active code version: ' + activeCodeVersion);
                    callback(activeCodeVersion);
                    break;
                }
            }
        })
    });

    req.on('error', function(e) {
        console.error(e);
        callback(e);
    });    

    req.end();
}

/** 
Return an array of code version folders that should be deleted from the instance.

@param {Object} options
@param {String} activeCodeVersion
@param {Function} callback - function used to return array of code versions that can be deleted
*/
function getCodeVersionFolders(options, activeCodeVersion, callback) {
    getCodeVersionHttpPage(options, function(codeVersionHttpPage) {
        var codeVersions = parseCodeVersions(activeCodeVersion, codeVersionHttpPage, options.max_code_versions);
    
        if (codeVersions.length) {
            console.log('Number of code version folders to delete: ' + codeVersions.length);
        }        

        callback(codeVersions);        
    })
}

/**
Parse the HTML for the code versions page and return just the names of the code
versions that are not active and should be deleted.

The regular expressions are explained below:

codeversion_re = /<a href=.*<tt>(.*)<\/tt>/g;

The 'codeversion_re' regex parses the HTML for the 'cartridges' HTTP page found at the following URL

https://myinstance.demandware.net/on/demandware.servlet/webdav/Sites/Cartridges

An example of the HTML returned for this page is this:

1.    <tr>
2.    <td align="left">&nbsp;&nbsp;
3.    <a href="/on/demandware.servlet/webdav/Sites/Cartridges/New_Version/"><tt>New_Version/</tt></a></td>
4.    <td align="right"><tt>&nbsp;</tt></td>
5.    <td align="right"><tt>Wed, 03 Dec 2014 15:43:51 GMT</tt></td>
6.    </tr>

The HTML markup on line 3 is what the regex is targeting. The regex breaks that line up into
a group to capture the code version name (.*), the code below loops through the matches 
and will use matches[i][1] to get the codeversion name and store it in an array.
    
codeversion_date_re = /<td align="right"><tt>([^.]*)<\/tt>/g

The codeversion_date_re regex parses the same HTML page (I know its double parsing but I could
not figure out a single regex to do this) and gets the codeversion date.

The rest of the code concerns sorting and filtering the array of codeversions that is returned.

@param {String} activeCodeVersion - the active code version
@param {String} codeVersionHttpPage - HTML of the page to be parsed
@param {Number} maxCodeVersions - the number of code versions to keep
*/
function parseCodeVersions(activeCodeVersion, codeVersionHttpPage, maxCodeVersions) {  
    var codeVersion, 
        codeVersions = [],
        codeversion_name_re = /<a href=.*<tt>(.*)<\/tt>/g,
        codeversion_date_re = /<td align="right"><tt>([^.]*)<\/tt>/g,
        matches = [],
        match;

    //Get the codeVersion names
    do {
        match = codeversion_name_re.exec(codeVersionHttpPage);                        
        match && matches.push(match);
    } while (match);

    if (matches.length) {
        for (var i = 0; i < matches.length; i++) {
            codeVersion = {
                name: matches[i][1],
            }
            codeVersions.push(codeVersion);
        };                      
    };

    //Get the codeVersion dates
    matches = [];
    do {
        match = codeversion_date_re.exec(codeVersionHttpPage);
        match && matches.push(match);
    } while (match);

    if (matches.length) {
        for (var i = 0; i < matches.length; i++) {
            var codeversion_date = matches[i][1];
            codeVersions[i].date = codeversion_date;
        };                      
    };

    codeVersions.sort(function(x,y) {
        var d1 = new Date(x.date);
        var d2 = new Date(y.date);
        return d1 - d2;
    });

    codeVersions = codeVersions.map(function(ele) {
        return ele.name;
    }).filter(function (ele) {
        return activeCodeVersion.indexOf(ele) === -1;                        
    });

    if (maxCodeVersions) {
        if (codeVersions.length > maxCodeVersions) {
            return codeVersions.reverse().slice(maxCodeVersions);
        } else {
            return []; //return empty array - we don't delete anything
        }
    } else { 
        return codeVersions;
    }
};    

/**
Make HTTP request to get the HTML for the /Cartridges page

@param {Object} options
@param {Function} callback - callback to invoke and return the HTML
*/
function getCodeVersionHttpPage(options, callback){
    var https = require('https');
    
    var req = https.get({
        rejectUnauthorized: false,
        host: options.webdav_server,
        path: '/on/demandware.servlet/webdav/Sites/Cartridges',
        auth: options.webdav_username + ':' + options.webdav_password
    }, function(resp) {
        var str = '';
        resp.on('data', function(chunk) {
            str += chunk;
        });
        resp.on('end', function() {
            callback(str);
        })
    });

    req.on('error', function(e) {
        console.error(e);
        callback(e);
    });       

    req.end();
}

/**
Make an HTTP request to delete a code version on the /Cartridges WebDav 

@param {Object} options
@param {String} codeVersion
@param {Function} callback  
*/
function deleteCodeVersionFolder(options, codeVersion, callback) {
    console.log('Deleting code version: ' + codeVersion);

    var https = require('https');
    var path = '/on/demandware.servlet/webdav/Sites/Cartridges/' + codeVersion;
    
    var req = https.request({
        rejectUnauthorized: false,
        host: options.webdav_server,
        path: path,
        auth: options.webdav_username + ':' + options.webdav_password,
        method: 'DELETE'
    }, function(resp) {
        var str = '';
        resp.on('data', function(chunk) {
            str += chunk;
        });
        resp.on('end', function() {
            callback(str);
        })
    });

    req.on('error', function(e) {
        console.error(e);
        callback(e);
    });     

    req.end();
}